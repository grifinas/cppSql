CREATE TABLE `users` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`parent_id` INT UNSIGNED NULL DEFAULT '0',
	`name` VARCHAR(50) NULL DEFAULT '0',
	`created_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `parent_id` (`parent_id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;

INSERT INTO `test`.`users` (`name`, `created_at`) VALUES ('John Doe', '2017-11-19 19:52:00');
INSERT INTO `test`.`users` (`name`, `created_at`) VALUES ('Johanesburg', '2017-11-19 19:52:34');
INSERT INTO `test`.`users` (`parent_id`, `name`, `created_at`) VALUES ('1', 'Tupakas Sakuraitis', '2017-11-19 19:52:51');
INSERT INTO `test`.`users` (`parent_id`, `name`, `created_at`) VALUES ('2', 'Jonas Stalins', '2017-11-19 19:53:08');