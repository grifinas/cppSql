#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/statement.h>

class mysqlConnectorWrapper
{
public:
	mysqlConnectorWrapper(const sql::SQLString& table_name);
	~mysqlConnectorWrapper();
	sql::ResultSet *query(const sql::SQLString& query);
private:
	void error(sql::SQLException &e);
	sql::Connection *con;

};