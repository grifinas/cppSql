#include <iostream>
#include <cppconn/resultset.h>

#include "mysqlConnectorWrapper.h"


int main(void)
{
    sql::ResultSet *res;

    mysqlConnectorWrapper *sql = new mysqlConnectorWrapper("test");

    res = sql->query("SELECT `u`.*, `p`.name AS `parent_name` FROM `users` `u` LEFT JOIN `users` `p` ON (`u`.parent_id = `p`.id)");
    while (res->next()) {
        std::cout << res->getString("name") << " " << res->getString("created_at") << std::endl;
    }
    delete res;
    std::cout << std::endl;

    return EXIT_SUCCESS;
}