#include <iostream>

#include "mysqlConnectorWrapper.h"

mysqlConnectorWrapper::mysqlConnectorWrapper(const sql::SQLString& table_name)
{
	try {
		this->con = get_driver_instance()->connect("tcp://127.0.0.1:3306", "root", "");
		this->con->setSchema(table_name);
	} catch (sql::SQLException &e) {
		this->error(e);
	}
}

mysqlConnectorWrapper::~mysqlConnectorWrapper()
{
	delete this->con;
}

sql::ResultSet *mysqlConnectorWrapper::query(const sql::SQLString& query)
{
	try {
		sql::Statement *stmt = this->con->createStatement();
		//0 safeguards
		sql::ResultSet *res = stmt->executeQuery(query);
		delete stmt;
		return res;
	} catch (sql::SQLException &e) {
		this->error(e);
	}
}

void mysqlConnectorWrapper::error(sql::SQLException &e)
{
	//this is not how this function should look like
	std::cout << std::endl << "# ERR: SQLException in " << __FILE__;
	std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
	std::cout << "# ERR: " << e.what();
	std::cout << " (MySQL error code: " << e.getErrorCode();
	std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	exit (EXIT_FAILURE);
}